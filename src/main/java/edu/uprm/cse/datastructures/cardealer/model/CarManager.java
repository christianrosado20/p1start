package edu.uprm.cse.datastructures.cardealer.model;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.uprm.cse.datastructures.cardealer.util.JsonError;
import edu.uprm.cse.datastructures.cardealer.util.NotFoundException;

@Path("/cars")
public class CarManager {
	private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();
	
	// A METHOD TO READ ALL CARS AS AN ARRAY OF CAR.
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] cars = new Car[carList.size()];
		for(int i = 0; i < cars.length; i++) {
			cars[i] = carList.get(i);
		}
		return cars;
	}
	// A METHOD TO READ A CAR WITH GIVEN ID.
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == (id)) {
				return carList.get(i);
			}
		}
		throw new NotFoundException();
	}

	
	
	// A METHOD TO ADD A NEW CAR TO THE SYSTEM
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		carList.add(car);
		return Response.status(Status.CREATED).build();
	}
	
	
	// A METHOD TO UPDATE AN EXISTING CAR IN THE SYSTEM.
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for(Car c: carList){
			if(c.getCarId() == car.getCarId()) {
				carList.remove(c);
				carList.add(car);
				return Response.status(Status.OK).build();
			}
		}
		return Response.status(Status.NOT_FOUND).build();
	}
	
	// A METHOD TO DELETE AN EXISTING CAR FROM THE SYSTEM.
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == id) {
				carList.remove(carList.get(i));
				return Response.status(Status.OK).build();
			}
		}
		throw new NotFoundException(new JsonError("ERROR!!!", "Car " + id + " not found!"));
	}
}
