package edu.uprm.cse.datastructures.cardealer.model;

public class CarList {
	
	private static final CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
	
	public static void resetCars() {
		carList.clear();
	}
}
