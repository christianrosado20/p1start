package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {

		@SuppressWarnings("hiding")
		private Node<E> nextNode;
		private Node<E> lastNode;

		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} else {
				throw new NoSuchElementException();
			}
		}

	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return previous;
		}

		public void setPrev(Node<E> last) {
			this.previous = last;
		}


	}

	private Node<E> header;
	private Comparator<E> comp;
	private int currentSize;

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	public CircularSortedDoublyLinkedList(Comparator<E> c) {
		this.header = new Node<>();
		header.setNext(header);
		header.setPrev(header);
		this.currentSize = 0;
		this.comp = c;
	}

	@Override
	public boolean add(E obj) {
		if (this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj,this.header,this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		} 
		Node<E> temp = header.getNext();
		while(temp != header) {
			if(comp.compare(obj, temp.getElement()) <= 0) {
				Node<E> newNode = new Node<E>(obj, temp, temp.getPrev());
				temp.getPrev().setNext(newNode);
				temp.setPrev(newNode);
				currentSize++;
				return true;
			}
			temp = temp.getNext();

		}
		Node<E> newNode = new Node<E>(obj, header, header.getPrev());
		header.getPrev().setNext(newNode);
		header.setPrev(newNode);
		currentSize++;
		return true;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		} 
		return this.remove(i);
	}

	@Override
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize) || this.isEmpty()) {
			throw new IndexOutOfBoundsException();
		} 
		Node<E> temp = header.getNext();
		for(int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		temp.getNext().setPrev(temp.getPrev());
		temp.getPrev().setNext(temp.getNext());
		temp.setElement(null);
		temp.setNext(null);
		temp.setPrev(null);
		currentSize--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if ((index < 0) || (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = this.getPosition(index);
		return temp.getElement();
	}

	private Node<E> getPosition(int index){
		int currentPosition=0;
		Node<E> temp = this.header.getNext();

		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;

	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		Node<E> temp = header.getNext();
		int count = 0;
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return count;
			}
			temp = temp.getNext();
			count++;
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		Node<E> temp = header.getPrev();
		int count = this.size()-1;
		while(temp != header) {
			if(temp.getElement().equals(e)) {
				return count;
			}
			temp = temp.getPrev();
			count--;
		}
		return -1;
	}



}
